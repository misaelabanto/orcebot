'use strict';

const Telegraf = require('telegraf');
const bot = new Telegraf('453758017:AAH9CBYWpbVJrBAIpG7vz-nCy08UHb2jWqc');
const http = require('http');
const md5 = require('./util/md5');
const queryString = require('querystring');
const plogin = '/login.json.php';
const pcursos = '/cursos.json.php';
const pnotas = '/notas.json.php';

var codigo = '';
var password = '';
var codCurso = '';
var nombreCurso = '';
var seccion = '';
var re_codigo = /[1-9]\d{7}[A-K]/;
var re_curso = /[A-Z][A-Z][0-9]{3}/;
var alumno = null;
var cursos = null;
var practicas = null;
var examenes = null;

//FLAGS:
var fcodigo = false;
var fpassword = false;
var fcurso = false;

var sessionCookie;

var i = 0;
var isValid = false;
var msg;

bot.start((ctx) => {
    console.log('started: ', ctx.from.id);
    return ctx.replyWithMarkdown('¡Bienvenido! Puedes empezar accediendo con /sesion');
});

function cancel(){
    fcodigo = false;
    fpassword = false;
}

function iniciarSesion(ctx){
    console.log('Entra, pero ya no puede hacer nada con el ... Wait!');
    ctx.replyWithMarkdown('Iniciando sesión...');
    var httpSesion = http.request(
        {
            host: 'www.orce.uni.edu.pe',
            port: 80,
            path: '/mobile/api/login.json.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Connection': 'keep-alive'
            }
        },
        (response) => {
            var r = '';
            response.on('data', (result) => {
                r += result;
            });
            response.on('end', () => {
                var res = JSON.parse(r);
                console.log(res);
                if(res.error){
                    ctx.replyWithMarkdown('Código o contraseña incorrectos');
                    cancel();
                }else{
                    ctx.replyWithMarkdown('Inicio de sesión exitoso\n' +
                        'Escribe /info para ver información del alumno\n' +
                        'Escribe /cursos para ver los cursos\n' +
                        'EScribe /notas para ver las notas');
                    alumno = res.data;
                    cancel();
                }
            });
            sessionCookie = response.headers['set-cookie'];
            console.log(`sessionCookie: ${sessionCookie}`);
        }
    );
    var dataSend = queryString.stringify({cod: codigo, psw: md5.md5(password), session: 1});
    console.log(`datasend: ${dataSend}`);
    httpSesion.write(dataSend);
    httpSesion.end();
}

function verCursos(ctx){
    ctx.replyWithMarkdown('Cargando cursos...');
    ctx.replyWithMarkdown('--CURSOS--');
    http.request(
        {
            host: 'www.orce.uni.edu.pe',
            port: 80,
            path: '/mobile/api/cursos.json.php',
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Cookie': sessionCookie
            }
        },
        (response) => {
            var r = '';
            response.on('data', (result) => {
                r += result;
            });
            response.on('end', () => {
                var res = JSON.parse(r);
                console.log(res);
                if(res.error){
                    ctx.replyWithMarkdown('No se pudo mostrar los cursos');
                }else{
                    cursos = res.data.cursos;
                    for(i = 0; i < cursos.length; i++){
                        console.log(cursos[i]);
                        ctx.replyWithMarkdown(
                            `*CODIGO:* ${cursos[i].codigo}\n*SECCION:* ${cursos[i].seccion}\n*NOMBRE:* ${cursos[i].nombre}\n*CONDICIÓN:* ${cursos[i].condicion}`
                        );
                    }
                }
            })
        }
    ).end();

}

function verNotas(ctx){
    ctx.reply('Cargando notas...');
    ctx.replyWithMarkdown(`--NOTAS DEL CURSO ${codCurso}::${nombreCurso}--`);
    var notasReq = http.request(
        {
            host: 'www.orce.uni.edu.pe',
            port: 80,
            path: '/mobile/api/notas.json.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Cookie': sessionCookie,
                'Connection': 'keep-alive'
            }
        },
        (response) => {
            var r = '';
            response.on('data', (result) => {
                r += result;
            });
            response.on('end', () => {
                var res = JSON.parse(r);
                if(res.error){
                    ctx.replyWithMarkdown('No se pudo mostrar las notas. Asegúrate de haber escrito un curso válido');
                }else{
                    practicas = res.data.PRACTICAS;
                    examenes = res.data.EXAMENES;
                    for(var i=0;i<practicas.length;i++){
                        msg = `*${practicas[i].NOMBRE}*\n\n*NOTA: *${practicas[i].NOTA}` + (practicas[i].RECLAMO ? `*RECLAMO:*${practicas[i].RECLAMO}` : '');
                        if(!practicas[i].APROB) msg += '\n\n  😞😞😞😞';
                        else msg += '\n\n  😀😀😀😀';
                        ctx.replyWithMarkdown(msg);
                    }
                    if(examenes.PARCIAL != null){
                        msg = `*EX. PARCIAL*\n\n*NOTA: *${examenes.PARCIAL.NOTA}` + (examenes.PARCIAL.RECLAMO ? `*RECLAMO:*${examenes.PARCIAL.RECLAMO}` : '');
                        if(!examenes.PARCIAL.APROB) msg += '\n\n  😞😞😞😞';
                        else msg += '\n\n  😀😀😀😀';
                        ctx.replyWithMarkdown(msg);
                    }
                    if(examenes.FINAL != null){
                        msg = `*EX. FINAL*\n\n*NOTA: *${examenes.FINAL.NOTA}` + (examenes.FINAL.RECLAMO ? `*RECLAMO:*${examenes.FINAL.RECLAMO}` : '');
                        if(!examenes.FINAL.APROB) msg += '\n\n  😞😞😞😞';
                        else msg += '\n\n  😀😀😀😀';
                        ctx.replyWithMarkdown(msg);
                    }
                    if(examenes.SUB != null){
                        msg = `*EX. SUSTI*\n\n*NOTA: *${examenes.SUB.NOTA}` + (examenes.SUB.RECLAMO ? `*RECLAMO:*${examenes.SUB.RECLAMO}` : '');
                        if(!examenes.FINAL.APROB) msg += '\n\n  😞😞😞😞';
                        else msg += '\n\n  😀😀😀😀';
                        ctx.replyWithMarkdown(msg);
                    }
                }
            })
        }
    );
    notasReq.write(queryString.stringify({CURSO: codCurso, SECCION: seccion}));
    notasReq.end();
    isValid = false;
}

bot.command('cancel', (ctx) => {
    fcodigo = false;
    fpassword = false;
    fcurso = false;
    ctx.replyWithMarkdown('La operación fue cancelada');
});

bot.command('sesion', (ctx) => {
    ctx.replyWithMarkdown('Envíame tu código');
    fcodigo = true;
    bot.on('text', (ctx1) => {
        if(fpassword){
            password = ctx1.message.text;
            iniciarSesion(ctx1);
        }
        if(fcodigo){
            if(re_codigo.test(ctx1.message.text.trim())){
                fcodigo = false;
                codigo = ctx1.message.text.trim();
                console.log(`codigo: ${codigo}`);
                ctx1.replyWithMarkdown('Envíame tu contraseña');
                fpassword = true;
            }else{
                ctx1.replyWithMarkdown('Código incorrecto, intenta nuevamente.\nEscribe /cancel para cancelar la operación');
            }
        }

    });
});

bot.command('info', (ctx) => {
    if(alumno != null){
        ctx.replyWithMarkdown('--INFORMACIÓN DEL ALUMNO--');
        ctx.replyWithMarkdown('*CÓDIGO:*\n' + alumno.CODIGO );
        ctx.replyWithMarkdown('*NOMBRE:*\n' + alumno.NOMBRE );
        ctx.replyWithMarkdown('*SITUACIÓN:*\n' + alumno.SITUACION_TXT );
        ctx.replyWithMarkdown('*CONDICIÓN:*\n' + alumno.CONDICION_TXT );
        ctx.replyWithMarkdown('*FACULTAD:*\n' + alumno.FAC_TXT);
    }else{
        ctx.replyWithMarkdown('Aún no has iniciado sesión.\nAccede con /sesion');
    }
});

bot.command('salir', (ctx) => {
    //todo: ejecutar cerrar sesión
});

bot.command('cursos', (ctx) => {
    if(alumno != null){
        verCursos(ctx)
    } else {
        ctx.replyWithMarkdown('Aún no has iniciado sesión.\nEscribe /sesion para acceder.');
    }
});

bot.command('notas', (ctx) => {
    if(cursos != null) {
        ctx.replyWithMarkdown('Envíame el código del curso.');
        fcurso = true;
    }else{
        ctx.replyWithMarkdown('Primero mira los cursos');
    }
});

bot.hears(/[H_h][O_o]+[L_l][A_a]+/, (ctx) => ctx.replyWithMarkdown('Hola, compañer@'));
bot.hears(/[A-Z_a-z]{2}[0-9]{3}/, (ctx1) => {
    if(fcurso){
        codCurso = ctx1.message.text;
        console.log(codCurso);
        if(re_curso.test(codCurso)){
            for(i=0;i<cursos.length;i++){
                if(codCurso === cursos[i].codigo){
                    nombreCurso = cursos[i].nombre;
                    seccion = cursos[i].seccion;
                    isValid = true;
                    fcurso = false;
                }
            }
            if(isValid){
                verNotas(ctx1);
            }
        }else{
            ctx1.replyWithMarkdown('El código es incorrecto. Intenta de nuevo. Escribe /cancel para cancelar');
        }
    }
});

bot.catch((err) => {
    console.log('Ooops', err)
});

bot.startPolling();

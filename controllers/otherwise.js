'use strict';

const Telegram = require('telegram-node-bot');
const TelegramBaseController = Telegram.TelegramBaseController;
const TextCommand = Telegram.TextCommand;
const tg = new Telegram.Telegram('YOUR_TOKEN');

class OtherwiseController extends TelegramBaseController {
    handle($){
        $.sendMessage('Sorry, I can\'t understand!');
    }
}

module.exports = OtherwiseController;
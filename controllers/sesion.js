'use strict';
const Telegram = require('telegram-node-bot');
const TelegramBaseController = Telegram.TelegramBaseController;
const http = require('http');
const queryString = require('querystring');
const md5 = require('../util/md5');

var re = /[1-9]\d{7}[A-K]/;

var options = {
    host: 'www.orce.uni.edu.pe',
    port: 80,
    path: '/mobile/api/login.json.php',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
};

var dataSession;
var sexoEmoji;

class SesionController extends TelegramBaseController {
    sesionHandler($) {
        const form = {
            cod: {
                q: 'Envíame tu código',
                error: 'Lo siento, el código no es válido',
                validator: (message, callback) => {
                    if(re.test(message.text.trim())) {
                        callback(true, message.text); //you must pass the result also
                        return;
                    }

                    callback(false);
                }
            },
            psw: {
                q: 'Envíame tu contraseña',
                error: 'Lo siento, no es válida',
                validator: (message, callback) => {
                    if(message.text) {
                        callback(true, md5.md5(message.text));
                        return
                    }

                    callback(false)
                }
            }
        };

        $.runForm(form, (result) => {
            var data = queryString.stringify(result);
            console.log(result);
            var httpRequest = http.request(options, (response) => {
                response.on('data', function(result){
                    dataSession = JSON.parse(result.toString()).data;
                    if(dataSession.SEXO === 'M') sexoEmoji = '👦';
                    else sexoEmoji = '👩';
                    $.runMenu({
                        message: 'Selecciona un curso',
                        options: {
                            parse_mode: 'Markdown'
                        },
                        oneTimeKeyboard: true,
                        'Datos del alumno': {
                            message: '\n' +
                            '--CÓDIGO:\n' + dataSession.CODIGO + '\n\n' +
                            '--NOMBRE:\n' + dataSession.NOMBRE + '\n\n' +
                            '--SITUACIÓN:\n' + dataSession.SITUACION_TXT + '\n\n' +
                            '--CONDICIÓN:\n' + dataSession.CONDICION_TXT + '\n\n' +
                            '--FACULTAD:\n' + dataSession.FAC_TXT + '\n\n'
                        },
                        'Notas': xd,
                        'Cursos': () => {

                        }
                    })
                });
            });
            httpRequest.write(data);
            httpRequest.end();
            $.runMenu({
                message: 'Selecciona una opción',
                options: {
                    parse_mode: 'Mardown'
                },
                'Datos del alumno': {
                    message: '🤪\n' +
                    '*CÓDIGO:* ' + dataSession.CODIGO +
                    '*NOMBRE:* ' + dataSession.NOMBRE +
                    '*SITUACIÓN:* ' + dataSession.SITUACION_TXT +
                    '*CONDICIÓN:* ' + dataSession.CONDICION_TXT +
                    '*FACULTAD:* ' + dataSession.FAC_TXT
                },
                'Ver cursos':{

                },
                'Exit': {
                    message: 'Do you realy want to exit?',
                    resizeKeyboard: true,
                    oneTimeKeyboard: true,
                    'yes': () => {

                    },
                    'no': () => {

                    }
                },
            })
        })
    }

    get routes() {
        return {
            'sesionCommand': 'sesionHandler'
        };
    }
}

module.exports = SesionController;
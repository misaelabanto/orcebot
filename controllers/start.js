'use strict';
const Telegram = require('telegram-node-bot');
const TelegramBaseController = Telegram.TelegramBaseController;

class StartController extends TelegramBaseController{
    startHandler($){
         $.sendMessage('¡Hola! Selecciona /sesion para iniciar sesión y poder visualizar tus notas');
    }

    get routes(){
        return {
            'startCommand':'startHandler'
        }
    }
}

module.exports = StartController;